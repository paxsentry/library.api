﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Library.API.Entities;
using Library.API.Helpers;
using Library.API.Models;
using Library.API.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Library.API.Controllers
{
    [Route("api/authors")]
    public class AuthorsController : Controller
    {
        private ILibraryRepository _repository;
        private IUrlHelper _urlHelper;
        private IPropertyMappingService _propertyMappingService;
        private ITypeHelperService _typeHelperService;

        public AuthorsController(
            ILibraryRepository repository,
            IUrlHelper urlHelper,
            IPropertyMappingService propertyMappingService,
            ITypeHelperService typeHelperService)
        {
            _repository = repository;
            _urlHelper = urlHelper;
            _propertyMappingService = propertyMappingService;
            _typeHelperService = typeHelperService;
        }

        [HttpGet(Name = "GetAuthors")]
        [HttpHead]
        public IActionResult GetAuthors(AuthorResourceParameters parameters, [FromHeader(Name = "Accept")] string mediaType)
        {
            if (!_propertyMappingService.ValidMappingExistsFor<AuthorDto, Author>(parameters.OrderBy)) { return BadRequest(); }
            if (!_typeHelperService.TypeHasProperties<AuthorDto>(parameters.Fields)) { return BadRequest(); }

            var authors = _repository.GetAuthors(parameters);

            var response = Mapper.Map<IEnumerable<AuthorDto>>(authors);

            if (mediaType.Equals("application/vnd.workshop144.hateoas+json"))
            {
                var paginationMetaData = new
                {
                    totalCount = authors.TotalCount,
                    pageSize = authors.PageSize,
                    currentPage = authors.CurrentPage,
                    totalPages = authors.TotalPages
                };

                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetaData));

                var links = CreateLinksForAuthors(parameters, authors.HasNext, authors.HasPrevious);

                var shapedData = response.ShapeData(parameters.Fields);

                var shapedAuthors = shapedData.Select(author =>
                {
                    var authorsAsDictionary = author as IDictionary<string, object>;
                    var authorLinks = CreateLinksForAuthor((Guid)authorsAsDictionary["Id"], parameters.Fields);

                    authorsAsDictionary.Add("links", authorLinks);

                    return authorsAsDictionary;
                });

                var linkedCollection = new
                {
                    value = shapedAuthors,
                    links
                };

                return Ok(linkedCollection);
            }
            else
            {
                var prevPage = authors.HasPrevious ? CreateAuthorsResourceUri(parameters, ResourceUriType.PreviousPage) : null;
                var nextPage = authors.HasNext ? CreateAuthorsResourceUri(parameters, ResourceUriType.NextPage) : null;

                var paginationMetaData = new
                {
                    totalCount = authors.TotalCount,
                    pageSize = authors.PageSize,
                    currentPage = authors.CurrentPage,
                    totalPages = authors.TotalPages,
                    prevPage,
                    nextPage
                };

                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetaData));

                return Ok(response.ShapeData(parameters.Fields));
            }
        }

        private string CreateAuthorsResourceUri(AuthorResourceParameters parameters, ResourceUriType type)
        {
            var pagenumber = parameters.PageNumber;
            switch (type)
            {
                case ResourceUriType.PreviousPage:
                    pagenumber--;
                    break;
                case ResourceUriType.NextPage:
                    pagenumber++;
                    break;
                case ResourceUriType.CurrentPage:
                default:
                    break;
            }

            return _urlHelper.Link("GetAuthors", new
            {
                fields = parameters.Fields,
                orderBy = parameters.OrderBy,
                searchQuery = parameters.SearchQuery,
                genre = parameters.Genre,
                pageNumber = pagenumber,
                pageSize = parameters.PageSize
            });
        }

        [HttpGet("{authorId}", Name = "GetAuthor")]
        public IActionResult GetAuthor(Guid authorId, [FromQuery] string fields)
        {
            if (!_typeHelperService.TypeHasProperties<AuthorDto>(fields)) { return BadRequest(); }

            var author = _repository.GetAuthor(authorId);
            if (author == null) { return NotFound(); }

            var response = Mapper.Map<AuthorDto>(author);

            var links = CreateLinksForAuthor(authorId, fields);
            var linkedResource = response.ShapeData(fields) as IDictionary<string, object>;

            linkedResource.Add("links", links);

            return Ok(linkedResource);
        }

        [HttpPost(Name = "CreateAuthor")]
        [RequestHeaderMatchesMediaType("Content-Type", new[] { "application/vnd.workshop144.author.full+json" })]
        public IActionResult CreateAuthor([FromBody] AuthorCreateDto dto)
        {
            if (dto == null) { return BadRequest(); }

            var newAuthor = Mapper.Map<Author>(dto);

            _repository.AddAuthor(newAuthor);

            if (!_repository.Save()) { throw new Exception("Creating author failed."); }

            var authorToReturn = Mapper.Map<AuthorDto>(newAuthor);

            var links = CreateLinksForAuthor(authorToReturn.Id, null);
            var linkedResource = authorToReturn.ShapeData(null) as IDictionary<string, object>;

            linkedResource.Add("links", links);

            return CreatedAtRoute("GetAuthor", new { authorId = linkedResource["Id"] }, linkedResource);
        }

        [HttpPost(Name = "CreateAuthorWithDateOfDeath")]
        [RequestHeaderMatchesMediaType("Content-Type", 
            new[] { "application/vnd.workshop144.authorwithdateofdeath.full+json",
                    "application/vnd.workshop144.authorwithdateofdeath.full+xml" })]
        //[RequestHeaderMatchesMediaType("Accept", new[] { })] // for output constraint - also can apply to the rest of the verbs
        public IActionResult CreateAuthorWithDateOfDeath([FromBody] AuthorCreateWithDateOfDeathDto dto)
        {
            if (dto == null) { return BadRequest(); }

            var newAuthor = Mapper.Map<Author>(dto);

            _repository.AddAuthor(newAuthor);

            if (!_repository.Save()) { throw new Exception("Creating author failed."); }

            var authorToReturn = Mapper.Map<AuthorDto>(newAuthor);

            var links = CreateLinksForAuthor(authorToReturn.Id, null);
            var linkedResource = authorToReturn.ShapeData(null) as IDictionary<string, object>;

            linkedResource.Add("links", links);

            return CreatedAtRoute("GetAuthor", new { authorId = linkedResource["Id"] }, linkedResource);
        }

        [HttpPost("{authorId}")]
        public IActionResult BlockAuthorCreation(Guid authorId)
        {
            if (_repository.AuthorExists(authorId)) { return new StatusCodeResult(StatusCodes.Status409Conflict); }

            return NotFound();
        }

        [HttpDelete("{authorId}", Name = "DeleteAuthor")]
        public IActionResult DeleteAuthor(Guid authorId)
        {
            var authorEntity = _repository.GetAuthor(authorId);
            if (authorEntity == null) { return NotFound(); }

            _repository.DeleteAuthor(authorEntity);

            if (!_repository.Save()) { throw new Exception($"Failed delete authot '{authorId}'"); }

            return NoContent();
        }

        [HttpOptions]
        public IActionResult GetAuthorOptions()
        {
            Response.Headers.Add("Allow", "GET,OPTIONS,POST");
            return Ok();
        }

        private IEnumerable<LinkDto> CreateLinksForAuthor(Guid authorId, string fields)
        {
            var links = new List<LinkDto>();

            var linkparam = string.Empty;
            if (!string.IsNullOrWhiteSpace(fields)) { linkparam = "fields = fields"; }

            links.Add(new LinkDto(
                _urlHelper.Link("GetAuthor", new { authorId = authorId, linkparam }),
                "self",
                "GET"));

            links.Add(new LinkDto(
                _urlHelper.Link("DeleteAuthor", new { authorId = authorId }),
                "delete_author",
                "DELETE"));

            links.Add(new LinkDto(
                _urlHelper.Link("CreateBookForAuthor", new { authorId = authorId }),
                "create_book_for_author",
                "POST"));

            links.Add(new LinkDto(
                _urlHelper.Link("GetBooksForAuthor", new { authorId = authorId }),
                "books",
                "GET"));

            return links;
        }

        private IEnumerable<LinkDto> CreateLinksForAuthors(AuthorResourceParameters parameters, bool hasNext, bool hasPrevious)
        {
            var links = new List<LinkDto>();

            links.Add(new LinkDto(CreateAuthorsResourceUri(parameters, ResourceUriType.CurrentPage), "self", "GET"));

            if (hasNext) { links.Add(new LinkDto(CreateAuthorsResourceUri(parameters, ResourceUriType.NextPage), "nextPage", "GET")); }
            if (hasPrevious) { links.Add(new LinkDto(CreateAuthorsResourceUri(parameters, ResourceUriType.PreviousPage), "previousPage", "GET")); }

            return links;
        }
    }
}