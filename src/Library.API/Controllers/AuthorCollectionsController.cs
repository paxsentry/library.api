﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Library.API.Entities;
using Library.API.Helpers;
using Library.API.Models;
using Library.API.Services;
using Microsoft.AspNetCore.Mvc;

namespace Library.API.Controllers
{
    [Route("api/authorcollections")]
    public class AuthorCollectionsController : Controller
    {
        private ILibraryRepository _repository;

        public AuthorCollectionsController(ILibraryRepository repository)
        {
            _repository = repository;
        }

        [HttpPost]
        public IActionResult CreateAuthorCollection([FromBody] IEnumerable<AuthorCreateDto> dto)
        {
            if (dto == null) { return BadRequest(); }

            var authorEntities = Mapper.Map<IEnumerable<Author>>(dto);

            foreach (var author in authorEntities)
            {
                _repository.AddAuthor(author);
            }

            if (!_repository.Save()) { throw new Exception("Error during saving Author collection."); }

            var response = Mapper.Map<IEnumerable<AuthorDto>>(authorEntities);
            var idsAsString = string.Join(",", response.Select(a => a.Id));

            return CreatedAtRoute("GetAuthorCollection", new { ids = idsAsString }, response);
        }

        [HttpGet("({ids})", Name = "GetAuthorCollection")]
        public IActionResult GetAuthorCollection([ModelBinder(BinderType = typeof(ArrayModelBinder))] IEnumerable<Guid> ids)
        {
            if (ids == null) { return BadRequest(); }

            var authorEntities = _repository.GetAuthors(ids);

            if (ids.Count() != authorEntities.Count()) { return NotFound(); }

            var response = Mapper.Map<IEnumerable<AuthorDto>>(authorEntities);

            return Ok(response);
        }
    }
}
