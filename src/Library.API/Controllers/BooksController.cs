﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Library.API.Entities;
using Library.API.Models;
using Library.API.Services;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Library.API.Controllers
{
    [Route("api/authors/{authorId}/books")]
    public class BooksController : Controller
    {
        private ILibraryRepository _repository;
        private ILogger<BooksController> _logger;
        private IUrlHelper _urlHelper;

        public BooksController(
            ILibraryRepository repository,
            ILogger<BooksController> logger,
            IUrlHelper urlHelper)
        {
            _repository = repository;
            _logger = logger;
            _urlHelper = urlHelper;
        }

        [HttpGet("", Name = "GetBooksForAuthor")]
        public IActionResult GetBooksForAuthor(Guid authorId)
        {
            if (!_repository.AuthorExists(authorId)) { return NotFound(); }

            var books = _repository.GetBooksForAuthor(authorId);

            var response = Mapper.Map<IEnumerable<BookDto>>(books);

            response = response.Select(book =>
            {
                book = CreateLinksForBook(book);
                return book;
            });

            var wrapper = new LinkedCollectionResourceWrapperDto<BookDto>(response);

            return Ok(CreateLinksForBooks(wrapper));
        }

        [HttpGet("{bookId}", Name = "GetBookForAuthor")]
        public IActionResult GetBookForAuthor(Guid authorId, Guid bookId)
        {
            if (!_repository.AuthorExists(authorId)) { return NotFound(); } // tested on this, but all returns 304 if not changed

            var book = _repository.GetBookForAuthor(authorId, bookId);
            if (book == null) { return NotFound(); }

            var response = Mapper.Map<BookDto>(book);

            return Ok(CreateLinksForBook(response));
        }

        [HttpPost(Name = "CreateBookForAuthor")]
        public IActionResult CreateBookForAuthor(Guid authorId, [FromBody] BookCreateDto dto)
        {
            if (dto == null) { return BadRequest(); }

            if (dto.Title.Equals(dto.Description))
            {
                ModelState.AddModelError(nameof(BookCreateDto), "The Title must be different that the description.");
            }

            if (!ModelState.IsValid) { return new Helpers.UnprocessableEntityObjectResult(ModelState); }

            if (!_repository.AuthorExists(authorId)) { return NotFound(); }

            var bookEntity = Mapper.Map<Book>(dto);

            _repository.AddBookForAuthor(authorId, bookEntity);

            if (!_repository.Save()) { throw new Exception($"Creating a book for author: '{authorId}' failed on Save."); }

            var bookToReturn = Mapper.Map<BookDto>(bookEntity);

            return CreatedAtRoute("GetBookForAuthor", new { authorId = bookToReturn.AuthorId, bookId = bookToReturn.Id }, CreateLinksForBook(bookToReturn));
        }

        [HttpDelete("{bookId}", Name = "DeleteBookForAuthor")]
        public IActionResult DeleteBookForAuthor(Guid authorId, Guid bookId)
        {
            if (!_repository.AuthorExists(authorId)) { return NotFound(); }

            var bookEntity = _repository.GetBookForAuthor(authorId, bookId);
            if (bookEntity == null) { return NotFound(); }

            _repository.DeleteBook(bookEntity);

            if (!_repository.Save()) { throw new Exception($"Deleting book '{bookId}' for author '{authorId}' failed."); }

            _logger.LogInformation(100, $"Book with id '{bookId}' deleted.");

            return NoContent();
        }

        [HttpPut("{bookId}", Name = "UpdateBookForAuthor")]
        public IActionResult UpdateBookForAuthor(Guid authorId, Guid bookId, [FromBody] BookUpdateDto dto)
        {
            if (dto == null) { return BadRequest(); }

            if (dto.Title.Equals(dto.Description))
            {
                ModelState.AddModelError(nameof(BookCreateDto), "The Title must be different that the description.");
            }

            if (!ModelState.IsValid) { return new Helpers.UnprocessableEntityObjectResult(ModelState); }

            if (!_repository.AuthorExists(authorId)) { return NotFound(); }

            var bookEntity = _repository.GetBookForAuthor(authorId, bookId);
            if (bookEntity == null)
            {
                var newBook = Mapper.Map<Book>(dto);
                newBook.Id = bookId;

                _repository.AddBookForAuthor(authorId, newBook);
                if (!_repository.Save()) { throw new Exception($"Upserting book '{bookId}' for author '{authorId}' failed."); }

                var response = Mapper.Map<BookDto>(newBook);

                return CreatedAtRoute("GetBookForAuthor", new { authorId = response.AuthorId, bookId = response.Id }, response);
            }

            Mapper.Map(dto, bookEntity);

            _repository.UpdateBookForAuthor(bookEntity);

            if (!_repository.Save()) { throw new Exception($"Updating book '{bookId}' for author '{authorId}' failed."); }

            return NoContent();
        }

        [HttpPatch("{bookId}", Name = "PartialUpdateBookForAuthor")]
        public IActionResult PartialUpdateBookForAuthor(Guid authorId, Guid bookId, [FromBody] JsonPatchDocument<BookUpdateDto> patchDoc)
        {
            if (patchDoc == null) { return BadRequest(); }
            if (!_repository.AuthorExists(authorId)) { return NotFound(); }

            var bookEntity = _repository.GetBookForAuthor(authorId, bookId);
            if (bookEntity == null)
            {
                return PartialUpsertBookForAuthor(authorId, bookId, patchDoc);
            }

            var bookToPatch = Mapper.Map<BookUpdateDto>(bookEntity);

            patchDoc.ApplyTo(bookToPatch, ModelState);

            if (bookToPatch.Title == bookToPatch.Description)
            {
                ModelState.AddModelError(nameof(BookCreateDto), "The Title must be different that the description.");
            }

            TryValidateModel(bookToPatch);

            if (!ModelState.IsValid) { return new Helpers.UnprocessableEntityObjectResult(ModelState); }

            Mapper.Map(bookToPatch, bookEntity);

            _repository.UpdateBookForAuthor(bookEntity);

            if (!_repository.Save()) { throw new Exception($"Updating book '{bookId}' for author '{authorId}' failed."); }

            return NoContent();
        }

        private IActionResult PartialUpsertBookForAuthor(Guid authorId, Guid bookId, JsonPatchDocument<BookUpdateDto> patchDoc)
        {
            var bookDto = new BookUpdateDto();
            patchDoc.ApplyTo(bookDto, ModelState);

            if (bookDto.Title == bookDto.Description)
            {
                ModelState.AddModelError(nameof(BookCreateDto), "The Title must be different that the description.");
            }

            TryValidateModel(bookDto);

            if (!ModelState.IsValid) { return new Helpers.UnprocessableEntityObjectResult(ModelState); }

            var bookToAdd = Mapper.Map<Book>(bookDto);
            bookToAdd.Id = bookId;

            _repository.AddBookForAuthor(authorId, bookToAdd);
            if (!_repository.Save()) { throw new Exception($"Partial upserting book '{bookId}' for author '{authorId}' failed."); }

            var response = Mapper.Map<BookDto>(bookToAdd);

            return CreatedAtRoute("GetBookForAuthor", new { authorId = response.AuthorId, bookId = response.Id }, response);
        }

        private BookDto CreateLinksForBook(BookDto book)
        {
            book.Links.Add(new LinkDto(
                _urlHelper.Link("GetBookForAuthor", new { bookId = book.Id }),
                "self",
                "GET"));

            book.Links.Add(new LinkDto(
                _urlHelper.Link("DeleteBookForAuthor", new { bookId = book.Id }),
                "delete_book",
                "DELETE"));

            book.Links.Add(new LinkDto(
                _urlHelper.Link("UpdateBookForAuthor", new { bookId = book.Id }),
                "update_book",
                "PUT"));

            book.Links.Add(new LinkDto(
                _urlHelper.Link("PartialUpdateBookForAuthor", new { bookId = book.Id }),
                "partial_update_book",
                "PATCH"));

            return book;
        }

        private LinkedCollectionResourceWrapperDto<BookDto> CreateLinksForBooks(LinkedCollectionResourceWrapperDto<BookDto> books)
        {
            books.Links.Add(new LinkDto(
                _urlHelper.Link("GetBooksForAuthor", new { }),
                "self",
                "GET"));

            return books;
        }
    }
}
