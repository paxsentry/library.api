﻿using System.ComponentModel.DataAnnotations;

namespace Library.API.Models
{
    public abstract class BookManipulationDto
    {
        [Required(ErrorMessage = "Title is mandatory!")]
        [MaxLength(100, ErrorMessage = "No more than 100 characters please.")]
        public string Title { get; set; }

        [MaxLength(500, ErrorMessage = "Please don't copy the whole book here. Maximum 500 characters.")]
        public virtual string Description { get; set; }
    }
}
