﻿using System.ComponentModel.DataAnnotations;

namespace Library.API.Models
{
    public class BookUpdateDto : BookManipulationDto
    {
        [Required(ErrorMessage = "Description is required!")]
        public override string Description { get => base.Description; set => base.Description = value; }
    }
}
