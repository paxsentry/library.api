﻿using System;
using System.Collections.Generic;
using System.Linq;
using Library.API.Services;
using System.Linq.Dynamic;

namespace Library.API.Helpers
{
    public static class IQueryableExtensions
    {
        public static IQueryable<T> ApplySort<T>(this IQueryable<T> source, string orderBy, Dictionary<string, PropertyMappingValue> mappingDictionary)
        {
            if (source == null) { throw new ArgumentNullException("source"); }
            if (mappingDictionary == null) { throw new ArgumentNullException("mappingDictionary"); }

            if (string.IsNullOrWhiteSpace(orderBy)) { return source; }

            var splitOrderBy = orderBy.Split(',');

            foreach (var item in splitOrderBy.Reverse())
            {
                var trimmedOrderBy = item.Trim();
                var orderDescending = trimmedOrderBy.EndsWith(" desc");
                var indexOfFirstSpace = trimmedOrderBy.IndexOf(" ");
                var propertyName = indexOfFirstSpace == -1 ? trimmedOrderBy : trimmedOrderBy.Remove(indexOfFirstSpace);

                if (!mappingDictionary.ContainsKey(propertyName)) { throw new ArgumentException($"Key mapping for '{propertyName}' is missing."); }

                var propertyMappingValue = mappingDictionary[propertyName];
                if (propertyMappingValue == null) { throw new ArgumentNullException("propertyMappingValue"); }

                foreach (var destination in propertyMappingValue.DestinationProperties.Reverse())
                {
                    if (propertyMappingValue.Revert) { orderDescending = !orderDescending; }

                    source = source.OrderBy(destination + (orderDescending ? " descending" : " ascending"));
                }
            }

            return source;
        }
    }
}
