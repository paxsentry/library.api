﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Reflection;

namespace Library.API.Helpers
{
    public static class IEnumerableExtensions
    {
        public static IEnumerable<ExpandoObject> ShapeData<TSource>(this IEnumerable<TSource> source, string fields)
        {
            if (source == null) { throw new ArgumentNullException("source"); }

            var expandoObjectList = new List<ExpandoObject>();
            var propertyInfoList = new List<PropertyInfo>();

            if (string.IsNullOrWhiteSpace(fields))
            {
                var propertyInfos = typeof(TSource).GetProperties(BindingFlags.Public | BindingFlags.Instance);

                propertyInfoList.AddRange(propertyInfos);
            }
            else
            {
                var fieldSplit = fields.Split(',');

                foreach (var field in fieldSplit)
                {
                    var propName = field.Trim();
                    var propInfo = typeof(TSource).GetProperty(propName, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);

                    if (propInfo == null) { throw new Exception($"Property '{propName}' was not found in '{typeof(TSource)}'"); }

                    propertyInfoList.Add(propInfo);
                }
            }

            foreach (TSource sourceObject in source)
            {
                var dataShapedObject = new ExpandoObject();

                foreach (var propInfo in propertyInfoList)
                {
                    var propValue = propInfo.GetValue(sourceObject);
                    ((IDictionary<string, object>)dataShapedObject).Add(propInfo.Name, propValue);
                }

                expandoObjectList.Add(dataShapedObject);
            }

            return expandoObjectList;
        }
    }
}
