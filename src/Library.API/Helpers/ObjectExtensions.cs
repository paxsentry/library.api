﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Reflection;

namespace Library.API.Helpers
{
    public static class ObjectExtensions
    {
        public static ExpandoObject ShapeData<TSource>(this TSource source, string fields)
        {
            if (source == null) { throw new ArgumentNullException("source"); }

            var dataShapedObject = new ExpandoObject();

            if (string.IsNullOrWhiteSpace(fields))
            {
                var propInfos = typeof(TSource).GetProperties(BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);

                foreach (var info in propInfos)
                {
                    var propValue = info.GetValue(source);
                    ((IDictionary<string, object>)dataShapedObject).Add(info.Name, propValue);
                }

                return dataShapedObject;
            }

            var fieldSplit = fields.Split(',');

            foreach (var field in fieldSplit)
            {
                var propName = field.Trim();
                var propInfo = typeof(TSource).GetProperty(propName, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);

                if (propInfo == null) { throw new Exception($"Property '{propName}' was not found in '{typeof(TSource)}'"); }

                var propValue = propInfo.GetValue(source);

                ((IDictionary<string, object>)dataShapedObject).Add(propInfo.Name, propValue);
            }

            return dataShapedObject;
        }
    }
}
