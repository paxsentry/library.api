﻿using System;
using System.Collections.Generic;
using System.Linq;
using Library.API.Entities;
using Library.API.Models;

namespace Library.API.Services
{
    public class PropertyMappingService : IPropertyMappingService
    {
        private Dictionary<string, PropertyMappingValue> _authorPropertyMapping =
            new Dictionary<string, PropertyMappingValue>(StringComparer.OrdinalIgnoreCase)
            {
                { "Id", new PropertyMappingValue(new List<string> {"Id" } ) },
                { "Genre", new PropertyMappingValue(new List<string> {"Genre" } ) },
                { "Age", new PropertyMappingValue(new List<string> {"DateOfBirth" }, true) },
                { "Name", new PropertyMappingValue(new List<string> {"FirstName", "LastName" } ) }
            };

        private IList<IPropertyMapping> propertyMappings = new List<IPropertyMapping>();

        public PropertyMappingService()
        {
            propertyMappings.Add(new PropertyMapping<AuthorDto, Author>(_authorPropertyMapping));
        }

        public Dictionary<string, PropertyMappingValue> GetPropertMappings<TSource, TDestination>()
        {
            var matchingMapping = propertyMappings.OfType<PropertyMapping<TSource, TDestination>>();

            if (matchingMapping.Count() == 1)
            {
                return matchingMapping.First()._mappingDictionary;
            }

            throw new Exception($"Cannot find exact property mapping instance for <{typeof(TSource)}>, <{typeof(TDestination)}>");
        }

        public bool ValidMappingExistsFor<TSource, TDestination>(string fields)
        {
            if (string.IsNullOrWhiteSpace(fields)) { return true; }

            var propertyMapping = GetPropertMappings<TSource, TDestination>();

            var fieldsSplit = fields.Split(',');

            foreach (var item in fieldsSplit)
            {
                var trimmed = item.Trim();
                var indexOfFirstSpace = trimmed.IndexOf(" ");
                var propertyName = indexOfFirstSpace == -1 ? trimmed : trimmed.Remove(indexOfFirstSpace);

                if (!propertyMapping.ContainsKey(propertyName)) { return false; }
            }

            return true;
        }
    }
}
