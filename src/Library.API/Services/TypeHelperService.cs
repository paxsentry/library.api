﻿using System.Reflection;

namespace Library.API.Services
{
    public class TypeHelperService : ITypeHelperService
    {
        public bool TypeHasProperties<T>(string fields)
        {
            if (string.IsNullOrWhiteSpace(fields)) { return true; }

            var fieldSplit = fields.Split(',');

            foreach (var field in fieldSplit)
            {
                var propName = field.Trim();
                var propInfo = typeof(T).GetProperty(propName, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);

                if (propInfo == null) { return false; }
            }

            return true;
        }
    }
}
